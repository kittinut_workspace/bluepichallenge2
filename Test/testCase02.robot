*** Settings ***
Resource     ../Resource/Variable.robot
Resource     ../Resource/Keyword.robot

*** Test Cases ***
Open to do list website
  Open website

Create new item 1
  Input new item1 name
  Click add new item btn
  Click to-do task tab
  Check name item1 in to-do task

Create new item 2
  Click add item tab
  Input new item2 name
  Click add new item btn
  Click to-do task tab
  Check name item2 in to-do task

Create new item 3
  Click add item tab
  Input new item3 name
  Click add new item btn
  Click to-do task tab
  Check name item3 in to-do task

Finish test01
  Click first item checkbox
  Click completed tab
  Check name item1 in completed

Delete test02 in to-do tab
  Click to-do task tab
  Click to-do first item delete button

Finish test03
  Click first item checkbox
  Click completed tab
  Check name item3 in completed

Delete test03 in completed tab
  Click completed tab
  Click second item delete button