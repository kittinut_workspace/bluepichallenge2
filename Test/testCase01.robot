*** Settings ***
Resource     ../Resource/Variable.robot
Resource     ../Resource/Keyword.robot

*** Test Cases ***
Open to do list website
  Open website

Create new item 1
  Input new item1 name
  Click add new item btn
  Click to-do task tab
  Check name item1 in to-do task

Finish test01
  Click first item checkbox
  Click completed tab
  Check name item1 in completed

Delete test01
  Click completed first item delete button