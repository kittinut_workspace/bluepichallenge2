*** Variables ***
${browser}      Chrome
${Url}   https://abhigyank.github.io/To-Do-List/


${item1_name}      test01
${item2_name}      test02
${item3_name}      test03

${firstItemToDo_id}    text-1
${secondItemToDo_id}    text-2
${thirdItemToDo_id}    text-3


${addItem_tab}        /html/body/div/div/div[1]/a[1]
${todo_tab}        /html/body/div/div/div[1]/a[2]
${Completed_tab}        /html/body/div/div/div[1]/a[3]

${itemName_input}        new-task

${firstItemToDo_checkbox}        //*[@id="incomplete-tasks"]/li/label/span[4]
${firstItemCompleted_name}        //*[@id="completed-tasks"]/li/span
${secondItemCompleted_name}        //*[@id="completed-tasks"]/li[2]/span
${firstItemTodoDelete_button}        //*[@id="1"]/span

${firstItemDeleteID_Button}        1
${secondItemDeleteID_button}        2
