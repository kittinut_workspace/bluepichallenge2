*** Settings ***
Library     Selenium2Library
Resource     ../Resource/Variable.robot

*** Keywords ***
Open website
  Open Browser  ${Url}   ${browser}

Input new item1 name
  Input Text  id=${itemName_input}  ${item1_name}
  Wait Until Element Does Not Contain    id=new-task    ${item1_name}

Click add new item btn
  Click Button  class=mdl-button

Click to-do task tab
  Click Element    xpath=${todo_tab}

Check name item1 in to-do task
  Element Should Contain    id=${firstItemToDo_id}    ${item1_name}

Click first item checkbox
  Click Element    xpath=${firstItemToDo_checkbox}

Click completed tab
  Click Element    xpath=${Completed_tab}

Check name item1 in completed
  Element Should Contain    xpath=${firstItemCompleted_name}    ${item1_name}

Click completed first item delete button
  Click button    id=${firstItemDeleteID_Button}

Click to-do first item delete button
  CLick Element    xpath=${firstItemTodoDelete_button}

Click add item tab
  Click Element    xpath=${addItem_tab}

Input new item 2 name
  Input Text  id=${itemName_input}  ${item2_name}
  Wait Until Element Does Not Contain    id=${itemName_input}    ${item2_name}

Check name item2 in to-do task
  Element Should Contain    id=${secondItemToDo_id}    ${item2_name}

Input new item3 name
  Input Text  id=${itemName_input}  ${item3_name}
  Wait Until Element Does Not Contain    id=${itemName_input}    ${item3_name}

Check name item3 in to-do task
  Element Should Contain    id=${thirdItemToDo_id}    ${item3_name}

Click second item delete button
  Click Element    id=${secondItemDeleteID_button}

Check name item3 in completed
  Element Should Contain    xpath=${secondItemCompleted_name}    ${item3_name}